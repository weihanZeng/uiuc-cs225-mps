/**
 * @file kdtree.cpp
 * Implementation of KDTree class.
 */

#include <utility>
#include <algorithm>
#include<cmath>

using namespace std;

template <int Dim>
bool KDTree<Dim>::smallerDimVal(const Point<Dim>& first,
                                const Point<Dim>& second, int curDim) const
{
    /**
     * @todo Implement this function!
     */
     if (first[curDim] == second[curDim])
         return first < second;
     else
         return first[curDim] < second[curDim];
}

template <int Dim>
bool KDTree<Dim>::shouldReplace(const Point<Dim>& target,
                                const Point<Dim>& currentBest,
                                const Point<Dim>& potential) const
{
    /**
     * @todo Implement this function!
     */
     return (distanceSquared(target, potential) < distanceSquared(target, currentBest)
             || (distanceSquared(target, potential) == distanceSquared(target, currentBest)
                 && potential < currentBest));
}

template <int Dim>
void KDTree<Dim>::KDhelp(int left, int right, int dim)
{
    int mid = (left+right)/2;
    if(left == right)
      return;
    quickSelect(left, right, mid, dim);
    if(left<(mid-1))
      KDhelp(left, (mid-1), (dim+1)%Dim);
    if((mid+1)<right)
      KDhelp((mid+1), right, (dim+1)%Dim);
}

template <int Dim>
int KDTree<Dim>::partition(int left, int right, int pIndex, int dim){
  Point<Dim> pivotValue = v[pIndex];
  std::swap(v[pIndex], v[right]);
  int storeIndex = left;
  for (int i = left; i < right; i++) {
    if(smallerDimVal(v[i], pivotValue, dim)){
      std::swap(v[storeIndex], v[i]);
      storeIndex++;
    }
  }
  std::swap(v[right], v[storeIndex]);
  return storeIndex;
}

template <int Dim>
void KDTree<Dim>::quickSelect(int left, int right, int mid, int dim){
  if(left >= right)
    return;
  int pivotIndex = (left+right)/2;
  pivotIndex = partition(left, right, pivotIndex, dim);
  if(mid == pivotIndex)
    return;
  else if(mid < pivotIndex)
    quickSelect(left, pivotIndex-1, mid, dim);
  else
    quickSelect(pivotIndex+1, right, mid, dim);
}

template <int Dim>
typename KDTree<Dim>::KDTreeNode* KDTree<Dim>::buildatree(KDTreeNode* node, vector<Point<Dim>> v, int start, int end){
  int mid =  (start+end)/2;
  node = new KDTreeNode(v[mid]);
  //node->point = v[mid];
  if(start == end){
    node->left = NULL;
    node->right = NULL;
    return node;
  }
  /*else if(){
    node->left = buildatree(node->left, v, start, mid-1);
    node->right = NULL;
  }*/
  else if(end-start==1){
    node->left = NULL;
    node->right = buildatree(node->right, v, mid+1, end);
  }
  else{
    node->left = buildatree(node->left, v, start, mid-1);
    node->right = buildatree(node->right, v, mid+1, end);
  }
  return node;
}

template <int Dim>
KDTree<Dim>::KDTree(const vector<Point<Dim>>& newv)
{
    /**
     * @todo Implement this function!
     */
     v = newv;
     if(v.empty())
      return;
    KDhelp(0, v.size()-1, 0);
    root = buildatree(root, v, 0, v.size()-1);
}

template <int Dim>
KDTree<Dim>::KDTree(const KDTree& other) {
  /**
   * @todo Implement this function!
   */
   size = other.size;
   root = NULL;
   for(int i = 0; i < other.v.size(); i++){
     v.push_back(other.v[i]);
   }
}
template <int Dim>
const KDTree<Dim>& KDTree<Dim>::operator=(const KDTree& rhs) {
  /**
   * @todo Implement this function!
   */

  return *this;
}

template <int Dim>
KDTree<Dim>::~KDTree() {
  /**
   * @todo Implement this function!
   */
}

template <int Dim>
Point<Dim> KDTree<Dim>::findNearestNeighbor(const Point<Dim>& query) const
{
    /**
     * @todo Implement this function!
     */

     int nearest = findNearest(query, 0, 0, v.size() - 1);
     return v[nearest];
}

template <int Dim>
int KDTree<Dim>::findNearest(const Point<Dim>& query, int dim, int left,
                             int right) const
{
    if (left >= right)
        return left;

    int mid = (right + left) / 2;
    int nearest, other;
    double radius2, split2;

    if (smallerDimVal(query, v[mid], dim)) {
        nearest = findNearest(query, (dim + 1) % Dim, left, mid - 1);
    } else {
        nearest = findNearest(query, (dim + 1) % Dim, mid + 1, right);
    }

    if (shouldReplace(query, v[nearest], v[mid]))
        nearest = mid;

    radius2 = distanceSquared(query, v[nearest]);

    split2 = (v[mid][dim] - query[dim]);
    split2 *= split2;

    if (radius2 >= split2) {
        if (smallerDimVal(query, v[mid], dim)) {

            other = findNearest(query, (dim + 1) % Dim, mid + 1, right);
        } else {
            other = findNearest(query, (dim + 1) % Dim, left, mid - 1);
        }
        if (shouldReplace(query, v[nearest], v[other]))
            nearest = other;
    }
    return nearest;
}

template <int Dim>
double KDTree<Dim>::distanceSquared(const Point<Dim>& first,
                                    const Point<Dim>& second) const
{
    double distsqr = 0;
    for (int i = 0; i < Dim; ++i)
        distsqr += (first[i] - second[i]) * (first[i] - second[i]);
    return distsqr;
}
