#include "Image.h"
#include <cmath>
#include <iostream>

void Image::lighten(){
  for (unsigned x = 0; x < this->width(); x++) {
      for (unsigned y = 0; y < this->height(); y++) {
        cs225::HSLAPixel & pixel = this->getPixel(x, y);
          pixel.l = pixel.l+0.1;
          if (pixel.l>=1) pixel.l=1;
          }
        }
      }
void Image::lighten(double amount){
  for (unsigned x = 0; x < this->width(); x++) {
      for (unsigned y = 0; y < this->height(); y++) {
        cs225::HSLAPixel & pixel = this->getPixel(x, y);
          pixel.l = pixel.l+amount;
          if (pixel.l>=1) pixel.l=1;
          }
        }
      }
void Image::darken(){
  for (unsigned x = 0; x < this->width(); x++) {
      for (unsigned y = 0; y < this->height(); y++) {
        cs225::HSLAPixel & pixel = this->getPixel(x, y);
          pixel.l = pixel.l-0.1;
          if (pixel.l<=0) pixel.l=0;
          }
        }
      }
void Image::darken(double amount){
  for (unsigned x = 0; x < this->width(); x++) {
      for (unsigned y = 0; y < this->height(); y++) {
        cs225::HSLAPixel & pixel = this->getPixel(x, y);
          pixel.l = pixel.l-amount;
          if (pixel.l<=0) pixel.l=0;
          }
        }
      }
void Image::saturate(){
  for (unsigned x = 0; x < this->width(); x++) {
      for (unsigned y = 0; y < this->height(); y++) {
        cs225::HSLAPixel & pixel = this->getPixel(x, y);
          pixel.s = pixel.s+0.1;
          if (pixel.s>=1) pixel.s=1;
          }
        }
      }
void Image::saturate(double amount){
  for (unsigned x = 0; x < this->width(); x++) {
      for (unsigned y = 0; y < this->height(); y++) {
        cs225::HSLAPixel & pixel = this->getPixel(x, y);
        pixel.s = pixel.s+amount;
        if (pixel.s>=1) pixel.s=1;
          }
        }
      }
void Image::desaturate(){
  for (unsigned x = 0; x < this->width(); x++) {
      for (unsigned y = 0; y < this->height(); y++) {
        cs225::HSLAPixel & pixel = this->getPixel(x, y);
          pixel.s = pixel.s-0.1;
          if (pixel.s<=0) pixel.s=0;
          }
        }
      }
void Image::desaturate(double amount){
  for (unsigned x = 0; x < this->width(); x++) {
      for (unsigned y = 0; y < this->height(); y++) {
        cs225::HSLAPixel & pixel = this->getPixel(x, y);
          pixel.s = pixel.s-amount;
          if (pixel.s<=0) pixel.s=0;
          }
        }
      }
void Image::grayscale(){
  for (unsigned x = 0; x < this->width(); x++) {
      for (unsigned y = 0; y < this->height(); y++) {
        cs225::HSLAPixel & pixel = this->getPixel(x, y);
          pixel.s = 0;
          }
        }
      }
void Image::rotateColor(double degrees){
  for (unsigned x = 0; x < this->width(); x++) {
    for (unsigned y = 0; y < this->height(); y++) {
      cs225::HSLAPixel & pixel = this->getPixel(x, y);
      pixel.h=pixel.h+degrees;
      while (pixel.h>360) pixel.h=pixel.h-360;
      while (pixel.h<0) pixel.h=pixel.h+360;
    }
  }
}
void Image::illinify(){
  for (unsigned x = 0; x < this->width(); x++) {
    for (unsigned y = 0; y < this->height(); y++) {
      cs225::HSLAPixel & pixel = this->getPixel(x, y);
      if(pixel.h > 113.5 && pixel.h < 293.5)
        pixel.h=216;
      else
        pixel.h=11;
        }
      }
}
void Image::scale(double factor){
    unsigned int originalWidth = this->width();
    unsigned int originalHeight = this->height();
    unsigned int newWidth = originalWidth * factor;
    unsigned int newHeight = originalHeight * factor;
    PNG* tempImage = new PNG (newWidth, newHeight);
    for (unsigned int i = 0; i < newHeight; i++){
        for (unsigned int j = 0; j < newWidth; j++){
            tempImage->getPixel(j,i).h = this->getPixel(j / factor,i / factor).h;
            tempImage->getPixel(j,i).s = this->getPixel(j / factor,i / factor).s;
            tempImage->getPixel(j,i).l = this->getPixel(j / factor,i / factor).l;
            tempImage->getPixel(j,i).a = this->getPixel(j / factor,i / factor).a;
        }
    }
    this->resize(newWidth, newHeight);
      for (unsigned int i = 0; i < newHeight; i++){
          for (unsigned int j = 0; j < newWidth; j++){
              this->getPixel(j,i).h = tempImage->getPixel(j,i).h;
              this->getPixel(j,i).s = tempImage->getPixel(j,i).s;
              this->getPixel(j,i).l = tempImage->getPixel(j,i).l;
              this->getPixel(j,i).a = tempImage->getPixel(j,i).a;
          }
      }
  }
void Image::scale(unsigned w, unsigned h){
    unsigned int originalWidth = this->width();
    unsigned int originalHeight = this->height();
    unsigned int widthRatio = originalWidth / w;
    unsigned int heightRatio = originalHeight / h;
    PNG* tempImage = new PNG (w, h);
    for (unsigned int i = 0; i < h; i++){
        for (unsigned int j = 0; j < w; j++){
            tempImage->getPixel(j,i).h = this->getPixel(j / widthRatio, i / heightRatio).h;
            tempImage->getPixel(j,i).s = this->getPixel(j / widthRatio, i / heightRatio).s;
            tempImage->getPixel(j,i).l = this->getPixel(j / widthRatio, i / heightRatio).l;
            tempImage->getPixel(j,i).a = this->getPixel(j / widthRatio, i / heightRatio).a;
        }
    }
    this->resize(w, h);
    for (unsigned int i = 0; i < h; i++){
        for (unsigned int j = 0; j < w; j++){
            this->getPixel(j,i).h = tempImage->getPixel(j,i).h;
            this->getPixel(j,i).s = tempImage->getPixel(j,i).s;
            this->getPixel(j,i).l = tempImage->getPixel(j,i).l;
            this->getPixel(j,i).a = tempImage->getPixel(j,i).a;
        }
    }
}
cs225::HSLAPixel * Image::operator()(unsigned x,unsigned y){
  return &(PNG::getPixel(x,y));
}
