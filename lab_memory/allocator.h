/**
 * @file allocator.h
 * @author Jack Toole, updated by Sean Massung to add doxygen comments
 * @date Fall 2011
 */

#ifndef ALLOCATOR_H
#define ALLOCATOR_H

#include <iostream>

#include "letter.h"
#include "room.h"

/**
 * An allocator object to wrap the allocation of last-name letter groups
 * to rooms.
 */
class Allocator
{
  public:
    /**
     * Creates an Allocator object based on a list of students and a
     *  list of rooms.
     * @param studentFile Path to roster file
     * @param roomFile Path to room list file
     */
    Allocator(const string& studentFile, const string& roomFile);


    /**
Public Member Functions | Private Member Functions | Private Attributes | List of all members
Allocator Class Reference

An allocator object to wrap the allocation of last-name letter groups to rooms. More...

#include "allocator.h"
Public Member Functions
 	Allocator (const string &studentFile, const string &roomFile)
 	Creates an Allocator object based on a list of students and a list of rooms. More...

void 	allocate ()
 	Calls the solving routine to allocate student letter groups to rooms. More...

void 	printStudents (std::ostream &stream=std::cout)
 	Prints each letter and how many students there are for each one. More...

void 	printRooms (std::ostream &stream=std::cout)
 	Prints the room allocation determined by this Allocator. More...

Private Member Functions
void 	createLetterGroups ()
 	Creates the array of letters. More...

void 	loadStudents (const string &file)
 	Reads the students from the students file and places them in their letter groups. More...

void 	loadRooms (const string &file)
 	Reads rooms from the rooms file and sets their capacities. More...

int 	solve ()
 	Runs the algorithm on the set of rooms and students. More...

Room * 	largestOpening ()
 	Return the room with the largest number of open seasts. More...

int 	minSpaceRemaining ()
 	Returns the amount of seats remaining in the room with the fewest spare seats. More...

Private Attributes
Letter * 	alpha
 	Array of all letters (size 26) More...

Room * 	rooms
 	Array of all rooms. More...

int 	roomCount
 	Size of the rooms array. More...

int 	studentCount
 	Total number of students. More...

int 	totalCapacity
 	Total number of available seats across all rooms. More...

Detailed Description

An allocator object to wrap the allocation of last-name letter groups to rooms.
Constructor & Destructor Documentation
Allocator::Allocator 	( 	const string &  	studentFile,
		const string &  	roomFile
	)

Creates an Allocator object based on a list of students and a list of rooms.

Parameters
    studentFile	Path to roster file
    roomFile	Path to room list file

Member Function Documentation
void Allocator::allocate 	( 		)

Calls the solving routine to allocate student letter groups to rooms.
void Allocator::printStudents 	( 	std::ostream &  	stream = std::cout	)

Prints each letter and how many students there are for each one.

Parameters
    stream	Where to print the results to

void Allocator::printRooms 	( 	std::ostream &  	stream = std::cout	)

Prints the room allocation determined by this Allocator.

Parameters
    stream	Where to print the results to

void Allocator::createLetterGroups 	( 		)
	private

Creates the array of letters.
void Allocator::loadStudents 	( 	const string &  	file	)
	private

Reads the students from the students file and places them in their letter groups.

Parameters
    file	The file to read the student list from

void Allocator::loadRooms 	( 	const string &  	file	)
	private

Reads rooms from the rooms file and sets their capacities.

Parameters
    file	The file to read the room list from

int Allocator::solve 	( 		)
	private

Runs the algorithm on the set of rooms and students.
Room * Allocator::largestOpening 	( 		)
	private

Return the room with the largest number of open seasts.
int Allocator::minSpaceRemaining 	( 		)
	private

Returns the amount of seats remaining in the room with the fewest spare seats.
Member Data Documentation
Letter* Allocator::alpha
	private

Array of all letters (size 26)
Room* Allocator::rooms
	private

Array of all rooms.
int Allocator::roomCount
	private

Size of the rooms array.

     * Calls the solving routine to allocate student letter
     *  groups to rooms.
     */
    void allocate();

    /**
     * Prints each letter and how many students there are for each one.
     *
     * @param stream Where to print the results to
     */
    void printStudents(std::ostream & stream = std::cout);

    /**
     * Prints the room allocation determined by this Allocator.
     *
     * @param stream Where to print the results to
     */
    void printRooms(std::ostream & stream = std::cout);

  private:
    /**
     * Creates the array of letters.
     */
    void createLetterGroups();

    /**
     * Reads the students from the students file and places them in
     *  their letter groups.
     * @param file The file to read the student list from
     */
    void loadStudents(const string& file);

    /**
     * Reads rooms from the rooms file and sets their capacities.
     * @param file The file to read the room list from
     */
    void loadRooms(const string& file);


    /**
     * Runs the algorithm on the set of rooms and students.
     */
    int solve();

    /**
     * Return the room with the largest number of open seasts
     */
    Room* largestOpening();

    /**
     * Returns the amount of seats remaining in the room with the
     *  fewest spare seats.
     */
    int minSpaceRemaining();

    /** Array of all letters (size 26) */
    Letter* alpha;

    /** Array of all rooms */
    Room* rooms;

    /** Size of the rooms array */
    int roomCount;

    /** Total number of students */
    int studentCount;

    /** Total number of available seats across all rooms */
    int totalCapacity;
};

#endif
