/**
 * @file avltree.cpp
 * Definitions of the binary tree functions you'll be writing for this lab.
 * You'll need to modify this file.
 */
template <class K, class V>
V AVLTree<K, V>::find(const K& key) const
{
    return find(root, key);
}

template <class K, class V>
V AVLTree<K, V>::find(Node* subtree, const K& key) const
{
    if (subtree == NULL)
        return V();
    else if (key == subtree->key)
        return subtree->value;
    else {
        if (key < subtree->key)
            return find(subtree->left, key);
        else
            return find(subtree->right, key);
    }
}

template <class K, class V>
void AVLTree<K, V>::rotateLeft(Node*& t)
{
    functionCalls.push_back("rotateLeft"); // Stores the rotation name (don't remove this)
    // your code here
    Node* node = t->right;
    t->right = node->left;
    node->left = t;
    if(heightOrNeg1(t->left) > heightOrNeg1(t->right))
      t->height = (heightOrNeg1(t->left))+1;
    else
      t->height = (heightOrNeg1(t->right))+1;
    t = node;
    if(heightOrNeg1(t->left) > heightOrNeg1(t->right))
      t->height = (heightOrNeg1(t->left))+1;
    else
      t->height = (heightOrNeg1(t->right));

    //node1->height = greater(heightOrNeg1(node1->left),heightOrNeg1(node1->right));
    //node2->height = greater(heightOrNeg1(node2->left),heightOrNeg1(node2->right));
}

template <class K, class V>
void AVLTree<K, V>::rotateLeftRight(Node*& t)
{
    functionCalls.push_back("rotateLeftRight"); // Stores the rotation name (don't remove this)
    // Implemented for you:
    rotateLeft(t->left);
    rotateRight(t);
}

template <class K, class V>
void AVLTree<K, V>::rotateRight(Node*& t)
{
    functionCalls.push_back("rotateRight"); // Stores the rotation name (don't remove this)
    // your code here
    Node* node = t->left;
    t->left = node->right;
    node->right = t;

    if(heightOrNeg1(t->left) > heightOrNeg1(t->right))
      t->height = (heightOrNeg1(t->left))+1;
    else
      t->height = (heightOrNeg1(t->right))+1;
    t = node;
    if(heightOrNeg1(t->left) > heightOrNeg1(t->right))
      t->height = (heightOrNeg1(t->left))+1;
    else
      t->height = (heightOrNeg1(t->right))+1;

    //node1->height = greater(heightOrNeg1(node1->left),heightOrNeg1(node1->right));
    //node2->height = greater(heightOrNeg1(node2->left),heightOrNeg1(node2->right));
}

template <class K, class V>
void AVLTree<K, V>::rotateRightLeft(Node*& t)
{
    functionCalls.push_back("rotateRightLeft"); // Stores the rotation name (don't remove this)
    // your code here
    rotateRight(t->right);
    rotateLeft(t);
}

template <class K, class V>
void AVLTree<K, V>::rebalance(Node*& subtree)
{
    // your code here
    if(subtree == NULL)
      return;
    if(subtree->left == NULL && subtree->right == NULL)
      return;
    Node* left = subtree->left;
    Node* right = subtree->right;
    int diff = heightOrNeg1(right) - heightOrNeg1(left);
    if(diff <= -2){
      int dif = heightOrNeg1(left->right) - heightOrNeg1(left->left);
      if(dif == 1)
        rotateLeftRight(subtree);
      if(dif == -1)
        rotateRight(subtree);
    }
    if(diff >= 2){
      int dif = heightOrNeg1(right->right) - heightOrNeg1(right->left);
      if(dif == 1)
        rotateLeft(subtree);
      if(dif == -1)
        rotateRightLeft(subtree);
    }
    if(heightOrNeg1(subtree->left) > heightOrNeg1(subtree->right))
      subtree->height = 1 + heightOrNeg1(subtree->left);
    else
      subtree->height = 1 + heightOrNeg1(subtree->right);



    //subtree->height = 1 + greater(heightOrNeg1(subtree->right), heightOrNeg1(subtree->left));
    return;
}

template <class K, class V>
void AVLTree<K, V>::insert(const K & key, const V & value)
{
    insert(root, key, value);
}

template <class K, class V>
void AVLTree<K, V>::insert(Node*& subtree, const K& key, const V& value)
{
    // your code here
    if(subtree == NULL){
      subtree = new Node(key, value);
      subtree->height = 0;
      return;
    }
    if(subtree->key < key){
      insert(subtree->right, key, value);
    }
    else{
      insert(subtree->left, key, value);
    }
    rebalance(subtree);
    return;
}

template <class K, class V>
void AVLTree<K, V>::remove(const K& key)
{
    remove(root, key);
}

template <class K, class V>
void AVLTree<K, V>::remove(Node*& subtree, const K& key)
{
    if (subtree == NULL)
        return;

    if (key < subtree->key) {
        // your code here
        remove(subtree->left, key);
        rebalance(subtree);
    } else if (key > subtree->key) {
        // your code here
        remove(subtree->right, key);
        rebalance(subtree);
    } else {
        if (subtree->left == NULL && subtree->right == NULL) {
            /* no-child remove */
            // your code here
            delete subtree;
            subtree = NULL;
            rebalance(subtree);
        } else if (subtree->left != NULL && subtree->right != NULL) {
            /* two-child remove */
            // your code here
            Node* node = subtree->left;
            while(node->right != NULL){
              node = node->right;
            }
              swap(subtree, node);
              remove(subtree->left, key);
              rebalance(subtree);

            

        } else {
            /* one-child remove */
            // your code here
            Node* left = subtree->left;
            Node* right = subtree->right;
            delete subtree;
            if(left)
              subtree = left;
            else
              subtree = right;
            rebalance(subtree);
        }
        // your code here

    }
    //if(subtree)
      //rebalance(subtree);
}

/*int greater(int x, int y){
  if(x>y)
    return x;
  return y;
}*/
